from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
engine = create_engine()
Session = sessionmaker()
session = Session()


engine = create_engine('mysql+mysqlconnector://root:@localhost:3306/perpustakaan', echo = True)
Session = sessionmaker(bind=engine)
session = Session()

if session:
    print("Connection Success")
    
class Customers(Base):
   __tablename__ = 'customers'
   id = Column(Integer, primary_key=True)
   username = Column(String)
   namaLengkap = Column(String)
   email = Column(String)

data = {
    "username" : 'RK',
    "namaLengkap" : 'Ravi Kumar', 
    "email" : 'ravi@gmail.com'
}

session.add(Customers(**data))
session.commit()